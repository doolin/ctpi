#!/usr/bin/env ruby

# invoke with ./roomba_robot.rb -f d
require 'rspec/autorun'

public

require 'pry'

# Consider passing memo as argument.
$memo = {}

def solution(x:, y:)
  return 0 if x == 1 && y == 1
  return 1 if x >= 2 && y == 1 || x == 1 && y >= 2
  key = [x, y]
  return $memo[key] if $memo[key]
  return $memo[[y, x]] if $memo[[y, x]]
  count = 2
  (1...x).each do |a|
    (1...y).each do |b|
      count += solution(x: x-a, y: y-b)
    end
  end
  $memo[key] = count
  $memo[[y, x]] = count
  count
end

private

# class documentation for rubocop
class RoombaRobot
  def self.solution(args)
    # code here
  end
end

RSpec.shared_examples 'roomba robot' do
  context '28 squares!' do
    it '' do
      size = 28
      # actual = 22750883079422934966181954039568885395604168260154104734000 # 100
      expect(described_class.solution(x: size, y: size)).to eq 1946939425648112
    end
  end

  context 'up to 5 squares in grid' do
    it '70 ways for (5 x 5) grid' do
      expect(described_class.solution(x: 5, y: 5)).to eq 70
    end
  end

  context 'up to 4 squares in grid' do
    it '20 ways for (4 x 4) grid' do
      expect(described_class.solution(x: 4, y: 4)).to eq 20
    end
  end

  context 'up to 3 squares in the grid' do
    it '1 way for (3 x 1) grid' do
      expect(described_class.solution(x: 3, y: 1)).to eq 1
    end

    it '1 way for (1 x 3) grid' do
      expect(described_class.solution(x: 1, y: 3)).to eq 1
    end

    it '3 ways for (3 x 2) grid' do
      expect(described_class.solution(x: 3, y: 2)).to eq 3
    end

    it '3 ways for (2 x 3) grid' do
      expect(described_class.solution(x: 2, y: 3)).to eq 3
    end

    it '6 ways for (3 x 3) grid' do
      expect(described_class.solution(x: 3, y: 3)).to eq 6
    end
  end

  context 'base cases' do
    it 'single square (1 x 1) grid' do
      expect(described_class.solution(x: 1, y: 1)).to eq 0
    end

    it '2 x 1 grid' do
      expect(described_class.solution(x: 2, y: 1)).to eq 1
    end

    it '1 x 2 grid' do
      expect(described_class.solution(x: 1, y: 2)).to eq 1
    end

    it '2 x 2 grid' do
      expect(described_class.solution(x: 2, y: 2)).to eq 2
    end
  end
end

RSpec.describe self do
  include_examples 'roomba robot'
end

RSpec.describe RoombaRobot do
  # include_examples 'roomba robot'
end
