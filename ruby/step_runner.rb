#!/usr/bin/env ruby

# invoke with ./step_runner.rb -f d
require 'rspec/autorun'

public

require 'pry'

$memo = []

def count(steps)
  # puts "steps: #{steps}"
  return 1 if steps == 0
  return 1 if steps == 1
  return 2 if steps == 2
  return $memo[steps] if $memo[steps]
  count = 0
  (1..3).each do |hop|
    count += count(steps-hop)
  end
  $memo[steps] = count
  count
end

private

# class documentation for rubocop
class StepRunner
  def self.count(steps)
    # code here
  end
end

RSpec.shared_examples 'step runner' do
  it '1 step for 1 way' do
    expect(described_class.count(1)).to eq 1
  end

  it '2 steps for 2 ways' do
    expect(described_class.count(2)).to eq 2
  end

  it '3 steps for 4 ways' do
    expect(described_class.count(3)).to eq 4
  end

  it '4 steps for 7 ways' do
    expect(described_class.count(4)).to eq 7
  end

  it '5 steps for 13 ways' do
    expect(described_class.count(5)).to eq 13
  end

  it '6 steps for 24 ways' do
    expect(described_class.count(6)).to eq 24
  end

  it '7 steps for 44 ways' do
    expect(described_class.count(7)).to eq 44
  end
end

RSpec.describe self do
  include_examples 'step runner'
end

RSpec.describe StepRunner do
  # include_examples 'step runner'
end
