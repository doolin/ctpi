#!/usr/bin/env ruby

# invoke with ./parentheses_validator.rb -f d
require 'rspec/autorun'

public

def solution(args)
  # code here
end

private

# class documentation for rubocop
class ParenthesesValidator
  def self.solution(args)
    # code here
  end
end

RSpec.shared_examples 'parentheses validator' do
  it 'solves' do
    expect(described_class.solution(['args'])).to be nil
  end
end

RSpec.describe self do
  include_examples 'parentheses validator'
end

RSpec.describe ParenthesesValidator do
  include_examples 'parentheses validator'
end
