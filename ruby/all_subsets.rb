#!/usr/bin/env ruby

# invoke with ./all_subsets.rb -f d
require 'rspec/autorun'

public

def all_subsets(args)
  subsets = []
  size = args.size
  for i in (0...(1<<size))
    puts "i: #{i.to_s(2)}"
    subset = []
    for j in (0...size)
      puts "  1 << j: #{(1<<j).to_s(2)}"
      if i & (1<<j) > 0
        subset << args[j]
      end
    end
    subsets << subset
  end
  subsets
end

# solution([1, 2, 3])
# solution('sshlgkj')

private

# class documentation for rubocop
class AllSubsets
  def self.solution(args)
    # code here
  end
end

RSpec.shared_examples 'all subsets' do
  it 'solves' do
    expect(described_class.all_subsets('ab')).to eq [[], ['a'], ['b'], ['a', 'b']]
  end
end

RSpec.describe self do
  include_examples 'all subsets'
end

RSpec.describe AllSubsets do
  # include_examples 'all subsets'
end
