#!/usr/bin/env ruby

# invoke with ./string_permutation.rb -f d
require 'rspec/autorun'

require 'pry'

class String

  def self.swap(ess, a, b)
    temp = ess[a]
    ess[a] = ess[b]
    ess[b] = temp
  end

  def self.permute(enn, ess)
    # binding.pry
    return [ess] if enn == 1
    result = []
    ess = ess.dup
    (0...(enn-1)).each do |i|
      permutation = permute(enn-1, ess)
      puts "Line #{__LINE__} permutation: #{permutation}"
      result << permutation
      enn.even? ? swap(ess, i, enn-1) : swap(ess, 0, enn-1)
    end

    permutation = permute(enn-1, ess)
    puts "Line #{__LINE__} permutation: #{permutation}"
    result << permutation
    result = result.flatten
    result
  end

  def permute
    # self.chars.permutation.to_a.map { |a| a.join }
    self.class.permute(self.size, self)
  end
end

RSpec.shared_examples 'string permutation' do
  it 'solves' do
    expect('a'.permute).to eq ['a']
  end

  it 'permutes "aa"' do
    expect('aa'.permute).to eq ['aa', 'aa']
  end

  it 'permutes "ab"' do
    expect('ab'.permute).to eq ['ab', 'ba']
  end

  it 'permutes "abc"' do
    expect('abc'.permute.sort).to eq [
      'abc', 'acb', 'bca', 'bac', 'cab', 'cba'
    ].sort
  end
end

RSpec.describe self do
  include_examples 'string permutation'
end
